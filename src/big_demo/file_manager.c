#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

#include "file_manager.h"


extern program_state_t* program_state;


//checks if the entered password is the same as the one stored in the file
//if the user exists and the password matches then returns user_t struct
//if it doesn't exist or something is wrong the return is NULL
user_t* user_import(char* name, char* password)
{
    FILE* file;
    int n_read;
    char file_path[100] = "";
    char stored_name[100] = "";
    char stored_password[100] = "";
    user_t* user = NULL;
    bool found = false;


    strcat(file_path, "./user_data/");
    strcat(file_path, name);

    file = fopen(file_path, "r");

    if(file != NULL)
    {
        do
        {
            if ((n_read = fscanf(file, "%s : %s", stored_name, stored_password)) != 2)
            {
                printf("<Error: invalid file>\n");
            }
            else
            {
                //TODO: add howdy's rsa and figure out how password authentican is safely done
                // Check if username matches 
                if(strcmp(name, stored_name) == 0)
                {
                    // Now check the passwords match
                    if(strcmp(password, stored_password) == 0)
                    {
                        // Authenticated, create the user struct and return it
                        

                        //check if user is in memory currently (still logged in)
                        //if user isn't in the current program state will need to load from file
                        for(int i = 0; i < program_state->user_count; i++)
                        {
                            if(strcmp(stored_name, program_state->users[i]->name) == 0)
                            {
                                user = program_state->users[i];
                                found = true;
                                printf("\n[ %s : Loaded From Memory ]\n\n", user->name);
                            }
                        }

                        //if the user isn't in memory
                        //create new memory allocation
                        //set name and import the tasks
                        //add them to program state
                        if(!found)
                        {
                            user = malloc(sizeof(user_t));
                            
                            user->name = malloc(sizeof(char) * 100);
                            strcpy(user->name, name);
                            tasks_import(user);
                            
                            printf("\n[ %s : Loaded From File ]\n\n", user->name);
                            program_state->users[program_state->user_count] = user;
                            program_state->user_count = program_state->user_count + 1;  
                        }
                        
                    }
                    else
                    {
                        printf("<Error: password incorrect>\n");
                    }
                }
                else
                {
                    printf("<Error: user not found>\n");
                }
            }
        }
        while(!feof(file));
        
        fclose(file);
    }
    else
    {
        printf("<Error: user file not found>\n");
    } 


    return user;
}


//import tasks from file
void tasks_import(user_t* user)
{
    FILE* file;
    char file_path[100] = "";
    task_t* new_task;
    char task_name[100] = "";
    char task_description[100] = "";
    char task_is_complete[100] = "";

    user->num_tasks = 0;
    user->tasks = malloc(sizeof(task_t*) * 100);
    
    strcat(file_path, "./user_data/");
    strcat(file_path, user->name);
    strcat(file_path, "_tasks");

    file = fopen(file_path, "r");
    

    if(file != NULL)
    {
        do
        {

            fscanf(file, "%s : %s : %s", task_name, task_description, task_is_complete);
            
            new_task = malloc(sizeof(task_t));

            //set task name
            new_task->name = malloc(sizeof(char) * 100);
            strcpy(new_task->name, task_name);
            
            //set task description
            new_task->description = malloc(sizeof(char) * 100);
            strcpy(new_task->description, task_description);
        
            //set is_complete based on last variable
            if(strcmp(task_is_complete, "0"))
            {
                new_task->is_complete = false;
            }
            else
            {
                new_task->is_complete = true;
            }

            user->tasks[user->num_tasks] = new_task;

            user->num_tasks = user->num_tasks + 1;
        }
        while(!feof(file));

        fclose(file);
    }
    else
    {
        printf("<Error: user tasks file not found>\n");    
    }
}

void tasks_export(user_t* user)
{
    FILE* file;
    char file_path[100] = "";
    task_t* task;

    //create file path
    strcat(file_path, "./user_data/");
    strcat(file_path, user->name);
    strcat(file_path, "_tasks");

    file = fopen(file_path, "w");

    if(file != NULL)
    {
        //print each task to file
        for(int i = 0; i < user->num_tasks; i++)
        {
            task = user->tasks[i];
            fprintf(file, "%s : %s : %d\n", task->name, task->description, task->is_complete);
        }

        fclose(file); 
    }
    else
    {
        printf("<Error: could not export user tasks>\n");    
    } 
}




