#ifndef PROGRAM_STATE_T_H
#define PROGRAM_STATE_T_H
#include "task_t.h"
#include "user_t.h"


//stores the running state of the program
//this means users can switch between each other without fully
//needing to reload their data from a file
//this is also import for uaf as the memory can still be storing
//other users, and a dangling pointer may be able to access that data
//without the need to login as that user
typedef struct program_state_t
{
    user_t** users;
    int user_count;
} program_state_t;

#endif 
