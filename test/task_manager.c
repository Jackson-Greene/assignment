#include "task_manager.h"


//adds a task to the array of tasks stored in the imported user
void add_task(user_t* user)
{
    int task_num = user->num_tasks;

    //create a new task and store it in the task array
    task_t* new_task = malloc(sizeof(task_t));
    user->tasks[task_num] = new_task;

    //allocate memory for the name and description on the heap
    new_task->name = malloc(sizeof(char) * 100);
    new_task->description = malloc(sizeof(char) * 100);

    //put the user input into the task name
    printf("input task name: ");
    scanf("%s", new_task->name);

    printf("==================\n");

    //put the user input into the task description
    printf("input task description: ");
    scanf("%s", new_task->description);
    printf("==================\n");
    

    user->num_tasks += 1;


    //printing the address for demonstration purpose
    printf("\n[ Task Address : %p ]\n\n", new_task);    
}



//removes a task selected from the imported user
//this is where a uaf exploit is triggered
void remove_task(user_t* user)
{
    //default task to remove is 0
    int task_to_remove = 1;
    bool error = true;
    bool in_range = false;

    //display all tasks before user can select
    display_tasks(user);

    printf("==================\n");
    printf("Enter a task to remove: ");
    

    //this checks if task_to_remove gets assigned by user input
    //if it does then there's no error
    if(scanf("%d", &task_to_remove) == 1)
    {
        error = false;
    }
    while (getchar() != '\n') {}
    fflush(stdin);
    printf("==================\n");

    //check if number is in range
    in_range = ((task_to_remove >= 1) && (task_to_remove <= user->num_tasks));

    if(error || !in_range)
    {
        printf("<Error: failed to remove task>\n");
    }

    //this is where the problem occurs
    //this should be inside the if below
    //if the user happens to enter for example "-"
    //error will = true but this free will still happen
    //the array shuffling code will not happen in that case though
    if(in_range)
    {
        free(user->tasks[(task_to_remove - 1)]);
    }
    //if there's no error then shuffle the
    //array down to cover the removed task
    if(!error && in_range)
    {
        for(int i = (task_to_remove - 1); i < user->num_tasks; i++)
        {
            user->tasks[i] = user->tasks[i + 1];
        }

        user->num_tasks = user->num_tasks - 1;

        printf("\n[ Freeing Task %d ]\n\n", (task_to_remove));
    }

}


//displays all the tasks for imported user
void display_tasks(user_t* user)
{
    int num_tasks;
    task_t** tasks;
    task_t* current_task;

    num_tasks = user->num_tasks;
    tasks = user->tasks;
    
    printf("\n%s's Tasks\n", user->name);

    //loop over tasks and print
    for(int i = 0; i < num_tasks; i++)
    {
        current_task = tasks[i];

        printf("--------TASK %d-----------------------------------------\n", (i + 1));
        printf("[ Task Address : %p ]\n", current_task);
        printf("Task Name: %s\nTask Description: %s\n", current_task->name, current_task->description);
        printf("--------------------------------------------------------\n");
    }
    printf("\n");
}