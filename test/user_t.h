#ifndef USER_T_H
#define USER_T_H
#include "task_t.h"

typedef void (*file_reader)();  
typedef void (*file_writer)();

//the user struct soters information about a single user
///their name and tasks
typedef struct user_t
{
    char* name;
    task_t** tasks;
    int num_tasks;
    char** reminders;
} user_t;

#endif 
