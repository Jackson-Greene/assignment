#include "test_file_manager.h"
void test_user_import()
{
    user_t* test_user = NULL;
    user_t* test_user2 = NULL;
    

    /*user_import will return NULL if user is not logged in
    OR if the username : password does not much any of the files in /user_data
    Thus, if we pass in a valid user and password, it should NOT return null*/
    test_user = user_import("test", "test");
    /*This SHOULD pass, since test : test is a valid user and pass*/
    assert(test_user != NULL);

    /*Now, we pass in a user and pass that are not found in /user_data*/
    test_user2 = user_import("invalid_username", "invalid_password");
    /*This SHOULD pass, since invalid_username : invalid_password 
    are not valid credentials, thus user_import should return NULL*/
    assert(test_user2 == NULL);
    
}
void test_tasks_import(user_t* user)
{
    tasks_import(user);
    /*Test file is test_tasks found in user_data directory 
    string in user->task->name should match what's in the file*/
    assert(strcmp(user->tasks[0]->name, "uniqueStringName1") == 0);


    /*String in user->task->name should match what's in the file*/
    assert(strcmp(user->tasks[0]->description, "uniqueStringDesc1") == 0);

    /*Test once more, for the next element in tasks[]*/
    assert(strcmp(user->tasks[1]->name, "uniqueStringName2") == 0);
    
    assert(strcmp(user->tasks[1]->description, "uniqueStringDesc2") == 0);
    
}

void test_tasks_export(user_t* user)
{
	char written_name[100] = "";
	char written_desc[100] = "";
	char written_completion[100] = "";

	int line_count = 0;
	FILE* file;
	file= fopen("./user_data/test_tasks", "r");
	while(fscanf(file, "%s : %s : %s", written_name, written_desc, written_completion) == 3)
	{
        /*Verify that the content written from user->tasks->name (as it does in
        task_export) is actually writing to the file properly*/
        assert(strcmp(user->tasks[line_count]->name, written_name) == 0);
        
        /*Verify that the content written from user->tasks->descrption (as
        done in task_export), is actually writing to the file properly*/
        assert(strcmp(user->tasks[line_count]->description, written_desc) == 0);
        /*We know the file format, and know that completion is 0 for all
        of test user's tasks*/
        assert(strcmp("0", written_completion) == 0);
		line_count++;
	}
}
	
