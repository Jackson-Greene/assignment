#include "test_task_manager.h"
void test_add_task(user_t* user)                                                                                                     
{
    add_task(user);
    /*Verify name read from script matches what's expected*/
    assert(strcmp(user->tasks[3]->name, "testName1") == 0); 

    /*verify description read from script matches what's expected*/
    assert(strcmp(user->tasks[3]->description, "testDesc1") == 0);

    /*Verify test_user->num_tasks = 1, since it should be incremented
    by 1 at the end of add_task()*/
    assert(user->num_tasks == 4);
    
    /*Repeat again for clarity*/
    add_task(user);

    /*Verify name read from script matches what's expected*/
    assert(strcmp(user->tasks[4]->name, "testName2") == 0);
    
    /*Verify description read from script matches what's expected*/
    assert(strcmp(user->tasks[4]->description, "testDesc2") == 0);

    assert(user->num_tasks ==5);
    
}


void test_remove_task(user_t* user)
{
    char* name = calloc(100, sizeof(char));

    printf("%d\n", user->num_tasks);
    /*Know it should equal 3, and should be reduced by 1 after remove_task()*/
    assert(user->num_tasks == 3);

    remove_task(user);
    assert(user->num_tasks == 2);

    name = user->tasks[0]->name;
    //the input is set as - so the task should not be removed
    remove_task(user);
    assert(user->num_tasks == 2);
    //checking the name is same before and after (it was not removed or modified)
    assert(strcmp(name, user->tasks[0]->name) == 0);
    
    /*THIS ASSERTION SHOULD FAIL*/
    //the task is not removed properly so name is not same as before   
}

void test_display_tasks(user_t* user)
{
    display_tasks(user);
    printf("EXPECTED: testName1 \t\t ACTUAL: %s\n", user->tasks[3]->name);
    printf("EXPECTED: testDesc1 \t\t ACTUAL: %s\n", user->tasks[3]->description);
    
    printf("EXPECTED: testName2 \t\t ACTUAL: %s\n", user->tasks[4]->name);
    printf("EXPECTED: testDesc2 \t\t ACTUAL: %s\n", user->tasks[4]->description);
}
