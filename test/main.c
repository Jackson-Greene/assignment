#include "main.h"


//global variable that stores the programs state throughout it's run time
program_state_t* program_state;

int main()
{
    //allocate memory for program state, 100 users max
    program_state = malloc(sizeof(program_state_t));
    program_state->user_count = 0;
    program_state->users = malloc(sizeof(user_t*) * 100);
    user_t* user;
    task_t** tasks;
    int num_tasks;

    printf("\nWelcome to Tasker\n");
    
    login_display();

    //free all users before exiting
    for(int i = 0; i < program_state->user_count; i++)
    {
        user = program_state->users[i];
        num_tasks = user->num_tasks;
        tasks = user->tasks;

        //loop over tasks and free their name and description
        for(int i = 0; i < num_tasks; i++)
        {
            free(tasks[i]->name);
            free(tasks[i]->description);
        }

        free(tasks);
        free(user->name);
        free(user);
    }

    return 0;
}


//menu that allows login or exit
void login_display()
{
    int initial_option;
    int menu_done = 0;
    char name_input[100];
    char password_input[100];
    user_t* user;

    do
    {
        printf("==================\n");
        printf("1. Login\n");
        printf("2. Quit\n");
        printf("==================\n");

        scanf("%d", &initial_option);
        while (getchar() != '\n') {}

        if (initial_option == 1)
        {
            printf("==================\n");
            printf("Name: ");
            scanf("%s", name_input);

            printf("Passord: ");
            scanf("%s", password_input);
            printf("==================\n");

            user = user_import(name_input, password_input);

            if(user != NULL)
            {
                menu_display(user);
            }
        }
        else if (initial_option == 2)
        {
            menu_done = true;
        }
        else
        {
            printf("<Error: invalid option>\n");
        }
    } while(!menu_done);
}


//main menu, calls functions from other
//files for task and save functonality
void menu_display(user_t* user)
{
    int user_choice = 0;
    int menu_done = 0;
    
    do
    {
        printf("==================\n");
        printf("1. Add Task\n");
        printf("2. Remove Task\n");
        printf("3. View Tasks\n");
        printf("4. Save Data\n");
        printf("5. Switch User\n");
        printf("6. Testing\n");
        printf("==================\n");

        printf("Choice: ");
        scanf("%d", &user_choice);
        while (getchar() != '\n') {}
        fflush(stdin);
        printf("==================\n");

        switch(user_choice)
        {
            case 1:
                add_task(user);
                break;
            case 2:
                remove_task(user);
                break;
            case 3:
                display_tasks(user);
                break;
            case 4:
                tasks_export(user);
                break;
            case 5:
                menu_done = true;
                break;
            case 6:
                test_add_task(user);
                test_display_tasks(user);
                test_tasks_import(user);
                test_tasks_export(user);
                test_user_import();
                /*This SHOULD fail in this version, since we fail to 
                set to NULL*/
                test_remove_task(user);
                break; 
            default:
                printf("<Error: invalid option>\n");
                break;
        }
    
    } while(!menu_done);
}
