#ifndef TEST_FILE_MANAGER_H
#define TEST_FILE_MANAGER_H
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "file_manager.h"
#include "user_t.h"
void test_user_import();
void test_tasks_import(user_t* user);
void test_tasks_export(user_t* user);
#endif
