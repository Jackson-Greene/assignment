#ifndef FILE_MANAGER_H
#define FILE_MANAGER_H

#include "user_t.h"
#include "program_state_t.h"

user_t* user_import(char* name, char* password);
void tasks_import(user_t*);
void tasks_export(user_t*);

#endif
