//remember to reference https://www.youtube.com/watch?v=3akzDXMVW8k
//explain ways function(data) might be called?

#include "demo.h"

int main() 
{
    data_t* old_data;
    data_t* new_data;

    //allocating memory on the heap for a data_t struct
    old_data = (data_t*)malloc(sizeof(data_t));
    //setting the text inside the Data struct
    strcpy(old_data->text, "text of data");

    //prints the the text in the data_t struct
    printf("First call of a_function\n");
    printf("Expected: 'text of data'\n");
    printf("Actual: ");
    a_function(old_data);

    //free the pointer
    free(old_data);
    //note that old_data (pointer) is not set to NULL
    //therefor after the free old_data (pointer) still stores the memory adress to the same address on the heap

    //allocating memory on the heap for another data_t struct (or something of the same size)
    //the memory on the heap where the old_data was stored is now replaced with new_data (as it can fit into the same memory place in the heap)
    new_data = (data_t*)malloc(sizeof(data_t));
    strcpy(new_data->text, "text of new_data");

    //problem in the code causes function to be called again with the freed pointer
    printf("\nSecond call of a_function\n");
    printf("Expected: 'text of data'\n");
    printf("Actual: ");
    a_function(old_data);


    //the function call results in "text of new_data" being printed despite not having new_data as import
    //beause data still points to it's original memory location and new_data has been allocated ontop of that
    //calling the function with the freed pointer is accessing the text in new_data
}


//prints text of data_t struct
void a_function(data_t* data)
{
    printf("%s\n", data->text);
}
