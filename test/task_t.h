#ifndef TASK_H
#define TASK_H

#include <stdbool.h>

//the task struct stores information about a single task
//it's name, description and if it has been complete yet
typedef struct task_t
{
    char* name;
    char* description;
    bool is_complete;
} task_t;

#endif
