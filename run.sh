#!/bin/bash

#script is entrypoint of docker container
#if test is an argument run the test version
#if no then just run the normal program
#if asan is defined then it runs program with address sanitizer

if [ "$1" == "test" ]; then
    echo "Running test program"
    cd test
    make clean
    make
    (cat ./testingscript.txt; cat -) | ./demo
elif [ "$1" == "asan" ]; then
    echo "Running asan enabled program"
    cd src/big_demo
    make clean
    make ASAN=1
    ./demo
else
    echo "Running normal program"
    cd src/big_demo
    make clean
    make
    ./demo
fi