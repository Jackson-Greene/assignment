////About the Program////

    This is a basic demo that has no other functionality other than
    showing a how a use after free exploit can be created



////How to Run the Program////

    Run "make" then run "./demo" (make sure you are in the basic demo directory)



////How to Trigger the Exploit////

    Simply make and run the program as described earlier and observe the output compared to what you would expect based on the code



////How to Detect the Exploit////

    To detect the exploit make and run the program with flag -asan <change stuff in dockerfile to enable this>





