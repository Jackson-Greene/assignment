#ifndef TASK_MANAGER_H
#define TASK_MANAGER_H
#include <stdio.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "user_t.h"

void add_task(user_t* user);
void remove_task(user_t* user);
void display_tasks(user_t* user);

#endif
