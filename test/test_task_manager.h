#ifndef TEST_TASK_MANAGER_H
#define TEST_TASK_MANAGER_H
#include <assert.h>
#include "task_manager.h"
#include "user_t.h"

void test_add_task(user_t* user);
void test_remove_task(user_t* user);
void test_display_tasks(user_t* user);

#endif
