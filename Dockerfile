FROM ubuntu:18.04

RUN apt-get update && apt-get upgrade -y
RUN apt-get install -y gcc make clang

RUN mkdir -p /home/program
COPY . /home/program
WORKDIR "/home/program"

RUN ["chmod", "+x", "/home/program/run.sh"]
ENTRYPOINT ["/home/program/run.sh"]