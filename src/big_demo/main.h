#ifndef MAIN_H
#define MAIN_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "file_manager.h"
#include "task_manager.h"
#include "user_t.h"
#include "program_state_t.h"

void login_display();
void menu_display(user_t* user);

#endif