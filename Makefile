# A sample Makefile to use make command to build, test and run the program
# Guide: https://philpep.org/blog/a-makefile-for-your-dockerfiles/
APP=isec3004.assignment

all: build

build:
	docker build --rm --tag=$(APP) .
	docker image prune -f

#runs the test version of program
test: build
	docker run -it --rm $(APP) test

#runs the address sanitizer version of program
asan: build
	docker run -it --rm $(APP) asan

#runs the normal program
run: build
	docker run -it --rm $(APP)

clean:
	docker image rm $(APP)
	docker system prune
